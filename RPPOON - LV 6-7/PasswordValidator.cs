﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_6_7
{
    class PasswordValidator
    {
        public StringChecker firstLink;
        public StringChecker lastLink;
        public PasswordValidator(StringChecker firstLink)
        {
            this.firstLink = new StringDigitChecker();
            this.firstLink.SetNext(firstLink);
            this.lastLink = firstLink;
        }
        public void AddLink(StringChecker link)
        {
                this.lastLink = this.lastLink.SetNext(link);
        }
        public bool CheckPassword(string stringToCheck)
        {
            return this.firstLink.Check(stringToCheck);
        }
    }
}