﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_6_7
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker stringChecker = new StringDigitChecker();
            StringChecker stringLowerCaseChecker = new StringLowerCaseChecker();
            StringChecker stringUpperCaseChecker = new StringUpperCaseChecker();
            StringChecker stringLengthChecker = new StringLengthChecker(6);
            StringChecker stringDigitChecker = new StringDigitChecker();

            PasswordValidator passwordValidator = new PasswordValidator(stringChecker);
            passwordValidator.AddLink(stringLowerCaseChecker);
            passwordValidator.AddLink(stringUpperCaseChecker);
            passwordValidator.AddLink(stringLengthChecker);
            passwordValidator.AddLink(stringDigitChecker);

            List<string> passwordsToCheck = new List<string>();
            passwordsToCheck.Add("Patlidzan");
            passwordsToCheck.Add("rotkvica0");
            passwordsToCheck.Add("Mrkvica98");
            passwordsToCheck.Add("rajcica22");
            passwordsToCheck.Add("PARADAJIZ5");
            passwordsToCheck.Add("Ne1");
            passwordsToCheck.Add("valjdajedobrosad56");
            passwordsToCheck.Add("000najsarafcigerastisarafciGerMedjunajsarafcigerastimsarafcigerimanasvijetu");
            passwordsToCheck.Add("kodajebitno)");
            passwordsToCheck.Add("neznamjanista?8");


            foreach (string word in passwordsToCheck)
            {
                if (passwordValidator.CheckPassword(word))
                {
                    Console.WriteLine("Lozinka " + word + " je odgovarajuća!!");
                }
                else
                {
                    Console.WriteLine("Lozinka " + word + " nije odgovarajuća!!");
                }
            }

        }
    }
}
