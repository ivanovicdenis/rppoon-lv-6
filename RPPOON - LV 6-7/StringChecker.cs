﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_6_7
{
    abstract class StringChecker
    {
        private StringChecker next;
        public StringChecker SetNext(StringChecker next)
        {
            this.next = next;
            return next;
        }
        public bool Check(string stringToCheck)
        {
            bool result = this.PerformCheck(stringToCheck);
            if (this.next != null && result == true)
            {
                return this.next.Check(stringToCheck);
            }
            return result;
        }
        protected abstract bool PerformCheck(string stringToCheck);
    }
}
