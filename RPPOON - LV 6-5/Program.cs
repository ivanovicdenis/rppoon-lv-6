﻿using System;

namespace RPPOON___LV_6_5
{
    enum MessageType
    {
        INFO = 1, // 1
        WARNING = 2, // 10
        ERROR = 4, //100
        ALL = 7 //111
    }
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            
            logger.SetNextLogger(fileLogger);

            logger.Log("TestError", MessageType.ERROR);
            logger.Log("TestInfo", MessageType.INFO);
            logger.Log("TestWarning", MessageType.WARNING);
        }
    }
}
