﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_6_5
{
    abstract class AbstractLogger
    {
        protected MessageType messageTypeHandling;
        protected AbstractLogger nextLogger;
        public AbstractLogger(MessageType messageType)
        {
            this.messageTypeHandling = messageType;
        }
        public AbstractLogger SetNextLogger(AbstractLogger logger)
        {
            this.nextLogger = logger;
            return nextLogger;
        }
        public void Log(string message, MessageType type)
        {
            if ((type & this.messageTypeHandling) != 0)
            {
                this.WriteMessage(message, type);
            }
            if (this.nextLogger != null)
            {
                this.nextLogger.Log(message, type);
            }
        }
        protected abstract void WriteMessage(string message, MessageType type);
    }
}
