﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPPOON___LV_6_6
{
    class StringLengthChecker : StringChecker
    {
        private int minLength;
        public StringLengthChecker(int minLength)
        {
            this.minLength = minLength;
        }
        protected override bool PerformCheck(string stringToCheck)
        {
            if (stringToCheck.Length>=minLength)
            {
                return true;
            }
            return false;
        }
    }
}
