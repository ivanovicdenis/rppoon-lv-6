﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPPOON___LV_6_6
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool result = stringToCheck.Any(char.IsLower);
            return result;
        }
    }
}
