﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_6_6
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker stringChecker = new StringDigitChecker();
            StringChecker stringLowerCaseChecker = new StringLowerCaseChecker();
            StringChecker stringUpperCaseChecker = new StringUpperCaseChecker();
            StringChecker stringLengthChecker = new StringLengthChecker(6);
            StringChecker stringDigitChecker = new StringDigitChecker();

            stringChecker
                .SetNext(stringLowerCaseChecker)
                .SetNext(stringUpperCaseChecker)
                .SetNext(stringLengthChecker)
                .SetNext(stringDigitChecker);

            List<string> wordsToCheck = new List<string>();
            wordsToCheck.Add("Patlidzan");
            wordsToCheck.Add("rotkvica0");
            wordsToCheck.Add("Mrkvica98");
            wordsToCheck.Add("rajcica22");
            wordsToCheck.Add("PARADAJIZ5");
            wordsToCheck.Add("Ne1");
            wordsToCheck.Add("valjdajedobrosad56");
            wordsToCheck.Add("000najsarafcigerastisarafciGerMedjunajsarafcigerastimsarafcigerimanasvijetu");
            wordsToCheck.Add("kodajebitno)");
            wordsToCheck.Add("neznamjanista?8");


            foreach (string word in wordsToCheck)
            {
                if (stringChecker.Check(word))
                {
                    Console.WriteLine("Riječ " + word + " je ispravna!!");
                }
                else
                {
                    Console.WriteLine("Riječ " + word + " nije ispravna!!");
                }
            }

            
        }
    }
}
