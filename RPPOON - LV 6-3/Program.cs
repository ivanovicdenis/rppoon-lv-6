﻿using System;

namespace RPPOON___LV_6_3
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem item = new ToDoItem("Gym", "Otić u gym", DateTime.Today);
            
            Console.WriteLine("Prije promjene: \n");
            Console.WriteLine(item.ToString());

            careTaker.AddCurentState(item.StoreState());
            item.ChangeTask("Učlaniti se u gym");

            Console.WriteLine("Nakon prve promjene: \n");
            Console.WriteLine(item.ToString());

            careTaker.AddCurentState(item.StoreState());
            item.ChangeTask("Neću ići nikako!");

            Console.WriteLine("Nakon druge promjene: \n");
            Console.WriteLine(item.ToString());

            item.RestoreState(careTaker.GetPreviousState());
            Console.WriteLine("Nakon nakon prvog vraćanja u prethodno stanje: \n");
            Console.WriteLine(item.ToString());

            item.RestoreState(careTaker.GetPreviousState());
            Console.WriteLine("Nakon nakon drugog vraćanja u prethodno stanje: \n");
            Console.WriteLine(item.ToString());
        }
    }
}
