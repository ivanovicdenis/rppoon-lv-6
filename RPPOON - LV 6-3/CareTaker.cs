﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_6_3
{
    class CareTaker
    {
        Stack<Memento> previusStates;
        public CareTaker()
        {
            previusStates = new Stack<Memento>();
        }
        public void AddCurentState(Memento state)
        {
            previusStates.Push(state);
        }
        public Memento GetPreviousState()
        {
            return previusStates.Pop();
        }
    }
}

