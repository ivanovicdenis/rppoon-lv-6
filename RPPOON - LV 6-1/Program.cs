﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace RPPOON___LV_6_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notes = new Notebook(new List<Note>() {
            new Note("777", "Linoderm"),
            new Note("Mikrokemija", "Urea"),
            });

            notes.AddNote(new Note("Utrošiti namjenska sredstva", "Potrebno je namjenski prema projektnom prijedlogu utrošiti odobrena sredstva od Središnjeg državnog ureda za obnovu i stambeno zbrinjavanje do kraja mjeseca"));
            notes.AddNote(new Note("Pokidati zaperke!!", "U svim plastenicima hitno treba pokidat sve zaperke, kako bi se ubrzalo dozrijevanje plodova."));
            notes.AddNote(new Note("Pripremiti projektni prijedlog", "Pripremiti projektni prijedlog zajedno da troškovnikom koji se odnosi na prokopavanje vrela bare Odmut. Projekt bi se trebao realizirati tijekom sušnog razdoblja."));

            

            IAbstractIterator iterator = notes.GetIterator();

            for (Note i = iterator.First();iterator.IsDone!=true; i=iterator.Next())
            {
                i.Show();
            }

        }

    }
}
