﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_6_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box boxes = new Box(new List<Product>() {
            new Product("Marmelada", 9.99),
            new Product("Pekmez", 10.99),
            });

            boxes.AddProduct(new Product("Džem", 11.98));
            boxes.AddProduct(new Product("Kompost", 15.84));



            IAbstractIterator iterator = boxes.GetIterator();

            for (Product i = iterator.First(); iterator.IsDone != true; i = iterator.Next())
            {
                Console.WriteLine(i.ToString());
            }

        }
    }
}
