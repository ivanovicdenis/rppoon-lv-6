﻿using System;

namespace RPPOON___LV_6_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Memento previousState;
            BankAccount account = new BankAccount("Denis Ivanović", "Gruzija", 753);

            Console.WriteLine("\nPrije promjene: \n");
            Console.WriteLine(account.ToString());

            previousState = account.StoreState();
            account.UpdateBalance(247);

            Console.WriteLine("\nNakon promjene: \n");
            Console.WriteLine(account.ToString());

            account.RestoreState(previousState);

            Console.WriteLine("\nNakon nakon vraćanja u prethodno stanje: \n");
            Console.WriteLine(account.ToString());

        }
    }
}
